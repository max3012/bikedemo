// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Bike.Demo
{
	[Register ("BikeDetailsViewController")]
	partial class BikeDetailsViewController
	{
		[Outlet]
		UIKit.UILabel CapacityLabel { get; set; }

		[Outlet]
		UIKit.UILabel ChargeTimeLabel { get; set; }

		[Outlet]
		UIKit.UICollectionView CollectionView { get; set; }

		[Outlet]
		UIKit.UILabel DescLabel { get; set; }

		[Outlet]
		UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		UIKit.UIPageControl PageControl { get; set; }

		[Outlet]
		UIKit.UILabel PriceLabel { get; set; }

		[Outlet]
		UIKit.UILabel VoltageLabel { get; set; }

		[Outlet]
		UIKit.UILabel WeightLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CapacityLabel != null) {
				CapacityLabel.Dispose ();
				CapacityLabel = null;
			}

			if (ChargeTimeLabel != null) {
				ChargeTimeLabel.Dispose ();
				ChargeTimeLabel = null;
			}

			if (CollectionView != null) {
				CollectionView.Dispose ();
				CollectionView = null;
			}

			if (DescLabel != null) {
				DescLabel.Dispose ();
				DescLabel = null;
			}

			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (PriceLabel != null) {
				PriceLabel.Dispose ();
				PriceLabel = null;
			}

			if (VoltageLabel != null) {
				VoltageLabel.Dispose ();
				VoltageLabel = null;
			}

			if (WeightLabel != null) {
				WeightLabel.Dispose ();
				WeightLabel = null;
			}

			if (PageControl != null) {
				PageControl.Dispose ();
				PageControl = null;
			}
		}
	}
}
