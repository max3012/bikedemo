﻿using System;
using System.Linq;
using FFImageLoading;
using Foundation;
using UIKit;

namespace Bike.Demo.Cells
{
    public partial class BikeTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("BikeTableViewCell");
        public static readonly UINib Nib;

        private Bike bike;

        static BikeTableViewCell()
        {
            Nib = UINib.FromName("BikeTableViewCell", NSBundle.MainBundle);
        }

        protected BikeTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        [Export("awakeFromNib")]
        public void AwakeFromNib()
        {
            DetailsButton.TouchUpInside += DetailsButton_TouchUpInside;
        }

        public void SetData(Bike bike)
        {
            this.bike = bike;

            NameLabel.Text = bike.BikeTitle;
            DescLabel.Text = bike.BikeCardDesc;
            PriceLabel.Text = bike.BikePlan;
            ImageService.Instance.LoadUrl(bike.BikeCardUrl).Into(Image);

            BatteryLabel.Text = bike.Features.ElementAtOrDefault(0)?.FeatureDes;
            SpeedLabel.Text = bike.Features.ElementAtOrDefault(1)?.FeatureDes;
            PowerLabel.Text = bike.Features.ElementAtOrDefault(2)?.FeatureDes;
        }

        private void DetailsButton_TouchUpInside(object sender, EventArgs e)
        {
            var mainNavigationController = UIApplication.SharedApplication.KeyWindow.RootViewController as UINavigationController;
            var storyboard = UIStoryboard.FromName("Main", null);
            var detailsVC = storyboard.InstantiateViewController(nameof(BikeDetailsViewController)) as BikeDetailsViewController;
            detailsVC.Bike = bike;
            mainNavigationController.PushViewController(detailsVC, true);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if(disposing)
            {
                DetailsButton.TouchUpInside -= DetailsButton_TouchUpInside;
            }
        }
    }
}
