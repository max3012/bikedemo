// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Bike.Demo.Cells
{
	[Register ("BikeTableViewCell")]
	partial class BikeTableViewCell
	{
		[Outlet]
		UIKit.UILabel BatteryLabel { get; set; }

		[Outlet]
		UIKit.UILabel DescLabel { get; set; }

		[Outlet]
		UIKit.UIButton DetailsButton { get; set; }

		[Outlet]
		UIKit.UIImageView Image { get; set; }

		[Outlet]
		UIKit.UILabel NameLabel { get; set; }

		[Outlet]
		UIKit.UILabel PowerLabel { get; set; }

		[Outlet]
		UIKit.UILabel PriceLabel { get; set; }

		[Outlet]
		UIKit.UIButton SelectButton { get; set; }

		[Outlet]
		UIKit.UILabel SpeedLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NameLabel != null) {
				NameLabel.Dispose ();
				NameLabel = null;
			}

			if (PriceLabel != null) {
				PriceLabel.Dispose ();
				PriceLabel = null;
			}

			if (DescLabel != null) {
				DescLabel.Dispose ();
				DescLabel = null;
			}

			if (Image != null) {
				Image.Dispose ();
				Image = null;
			}

			if (DetailsButton != null) {
				DetailsButton.Dispose ();
				DetailsButton = null;
			}

			if (SelectButton != null) {
				SelectButton.Dispose ();
				SelectButton = null;
			}

			if (BatteryLabel != null) {
				BatteryLabel.Dispose ();
				BatteryLabel = null;
			}

			if (SpeedLabel != null) {
				SpeedLabel.Dispose ();
				SpeedLabel = null;
			}

			if (PowerLabel != null) {
				PowerLabel.Dispose ();
				PowerLabel = null;
			}
		}
	}
}
