﻿using System;
using FFImageLoading;
using Foundation;
using UIKit;

namespace Bike.Demo.Cells
{
    public partial class ImageCollectionViewCell : UICollectionViewCell
    {
        public static readonly NSString Key = new NSString("ImageCollectionViewCell");
        public static readonly UINib Nib;

        static ImageCollectionViewCell()
        {
            Nib = UINib.FromName("ImageCollectionViewCell", NSBundle.MainBundle);
        }

        protected ImageCollectionViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void SetData(string url)
        {
            ImageService.Instance.LoadUrl(url).Into(Image);
        }
    }
}
