﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bike.Demo.Cells;
using Foundation;
using UIKit;

namespace Bike.Demo.Sources
{
    public class BikeTableSource : UITableViewSource
    {
        List<Bike> TableItems;
        string CellIdentifier = BikeTableViewCell.Key;

        public BikeTableSource(List<Bike> items)
        {
            TableItems = items;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(CellIdentifier) as BikeTableViewCell;
            var item = TableItems.ElementAt(indexPath.Row);

            cell.SetData(item);

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return TableItems?.Count ?? 0;
        }
    }
}
