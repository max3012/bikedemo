﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bike.Demo
{
    public partial class Bike
    {
        [JsonProperty("BikeCardDesc")]
        public string BikeCardDesc { get; set; }

        [JsonProperty("BikeCardUrl")]
        public string BikeCardUrl { get; set; }

        [JsonProperty("BikeDescription")]
        public string BikeDescription { get; set; }

        [JsonProperty("BikeImageUrls")]
        public List<string> BikeImageUrls { get; set; }

        [JsonProperty("BikePlan")]
        public string BikePlan { get; set; }

        [JsonProperty("BikeSpecfications")]
        public BikeSpecfications BikeSpecfications { get; set; }

        [JsonProperty("BikeTitle")]
        public string BikeTitle { get; set; }

        [JsonProperty("Features")]
        public List<Feature> Features { get; set; }
    }

    public partial class BikeSpecfications
    {
        [JsonProperty("Capacity")]
        public string Capacity { get; set; }

        [JsonProperty("Weight")]
        public string Weight { get; set; }

        [JsonProperty("Charge time")]
        public string ChargeTime { get; set; }

        [JsonProperty("Voltage")]
        public string Voltage { get; set; }
    }

    public partial class Feature
    {
        [JsonProperty("FeatureDes")]
        public string FeatureDes { get; set; }

        [JsonProperty("FeatureUrl")]
        public Uri FeatureUrl { get; set; }
    }
}
